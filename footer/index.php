<footer>
    <div class="container-fluid">
        <div class="row d-md-flex">
            <div class="col-12 col-md-3 ">
                <img class="img-fluid" src="./img/logo/shopempreendedor_retina.png" alt="">
            </div>
            <div class="col-12 col-md-3">
                <h4>Shop Empreendedor</h4>
                <p>O Shop Empreendedor nasceu para oferecer as melhores soluções em vídeos personalizados para você divulgar o seu produto ou serviço de forma eficiente e direta.</p>
                <a href="./quemsomos.php">Conheça mais</a>
            </div>
            <div class="col-12 col-md-3">
                <h4>Informações</h4>
                <ul>
                    <li><a href="">Shop Explica</a></li>
                    <li><a href="">Política de Privacidade</a></li>
                    <li><a href="">Formas de Pagamento</a></li>
                    <li><a href="">Licença de Uso</a></li>
                    <li><a href="">Termos de Uso</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-3 contato">
                <h3 class="font-weight-bold">Com dúvidas?               
                    Então entre em 
                    contato conosco!
                </h3>
                <a href="" class="botao-fale-conosco">Fale Conosco</a>
                
            </div>
            <div class="col-12 text-md-center direitos">
                <p>
				    2019©Shop Empreendedor. Todos os direitos reservados.<a href="http://www.setecurios.com.br/">Desenvolvimento 7 curiós
                  </a> </p>                
            </div>            
        </div>
    </div>
</footer>


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="./js/jquery-3.4.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="./bootstrap/js/bootstrap.min.js" ></script>
    <script src="./js/slick.min.js"></script>
    <script src="./js/main.js"></script>
    <script src="./js/jquery.mask.js"></script>
  </body>
</html>