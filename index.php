<?php
    include 'header/index.php';
    include 'header/menu.php';
?>

  <main>
      <div class="container-fluid position-relative comprar">
           <div class="row">
                <div class="col-12 d-flex position-absolute botoes justify-content-lg-end">
                   <a href="" class="btns btn-comprar"> <i class="fas fa-shopping-cart"></i> COMPRE AGORA</a>
                   <a href="" class="btns btn-quem"> <i class="fas fa-trophy"></i> QUEM SOMOS</a>
                </div>
           </div> 
      </div>
      <div class="container-fluid melhor-video"> <!--Inicio container video  -->
           <div class="row">
                <div class="col-12 col-lg-6 btns">
                    <h1 class="d-none d-lg-block">Saiba qual o melhor tipo de vídeo para o seu negócio.</h1>
                    <h4 class="d-none d-lg-block">Um vídeo pode servir para vários fins. Por isso, é muito importante você saber qual tipo de vídeo pode se encaixar melhor a sua necessidade. </h4>
                    <a href="" class=" btn d-lg-block d-none btn-primary">Assista ao vídeo e entenda melhor</a>
                    <a href="" class="btn d-lg-none btn-primary btn-saiba">Saiba qual o melhor tipo de vídeo <i class="fas fa-video"></i> </a>
                </div>
           </div> 
      </div><!--Fim container video  -->
      <div class="container-fluid  muito-simples"> <!--Inicio container dos numeros  -->
           <div class="row">
                <div class="col-12 simples">
                    <h2>É tudo muito simples.</h2>
                    <p class="text-center text-white" > São apenas seis passos até o seu vídeo!</p>
                    
                </div>
                <div class="col-12 col-md-6 col-lg-4  text-center">
                    <img src="./img/numeros/1.png" class="img-fluid text-center" alt="">
                    <p ><strong>Escolhendo seu vídeo</strong></p>
                    <p >Assista nossos modelos de vídeos e escolha aquele que mais se encaixa a sua necessidade e ao seu modelo de negócio</p>
                    
                </div>
                <div class="col-12 col-md-6 col-lg-4 text-center"><!-- numero 2  -->
                    <img src="./img/numeros/2.png" class="img-fluid" alt="">
                    <p > <strong> Tempo do Vídeo </strong></p>
                    <p >Defina a duração do vídeo que você irá precisar e que se enquadra ao seu negócio, podendo ser: 15 segundos, 30 segundos ou 60 segundos.</p>
                    
                </div>
                <div class="col-12 col-md-6 col-lg-4 text-center"><!-- numero 3  -->
                    <img src="./img/numeros/3.png" class="img-fluid" alt="">
                    <p ><strong>Acessórios</strong></p>
                    <p >Você pode escolher serviços extras para incrementar o seu vídeo: como uma locução. animação da sua logomarca e muito mais</p>
                    
                </div>
                <div class="col-12 col-md-6 col-lg-4 text-center"><!-- numero 4  -->
                    <img src="./img/numeros/4.png" class="img-fluid" alt="">
                    <p ><strong>Envio do material</strong></p>
                    <p >Após a confirmação de compra, você receberá uma mensagem com as informações e materiais necessários para produção do vídeo escolhido.</p>
                    
                </div>
                <div class="col-12 col-md-6 col-lg-4 text-center"><!-- numero 5  -->
                    <img src="./img/numeros/5.png" class="img-fluid" alt="">
                    <p ><strong>Finalizando a compra</strong></p>
                    <p >Confira as suas escolhas e finalize a compra efetuando o pagamento usando o PagSeguro com cartão de crédito, débito ou boleto. </p>
                    
                </div>
                <div class="col-12 col-md-6 col-lg-4 text-center"><!-- numero 6  -->
                    <img src="./img/numeros/6.png" class="img-fluid" alt="">
                    <p ><strong>Produção do vídeo</strong></p>
                    <p >Com todos os materiais em mãos, o vídeo será produzido e entregue em até 10 dias úteis. E você poderá fazer o download! </p>
                    
                </div>
           </div> 
      </div><!--Fim container dos numeros  -->
      <div class="container botao-entenda"><!-- inicio Botao  -->
            <div class="row">
                <div class="col-12 d-flex justify-content-center">
                    <a href="">Assista ao vídeo e entenda melhor <i class="fas fa-play"></i></a>
                </div>
            </div>
      </div><!-- Fim Botao  -->
      <div class="container-fluid categorias"><!-- inicio Categorias  -->
            <div class="row text-center box-categoria">
                <div class="col-12">
                    <h2>Algumas de nossas categorias de vídeos</h2>
                </div>
                <div class="col-6 col-md-4 col-lg-2 col-lg-2 "><!-- Beleza  -->
                    <img class="teste" src="./img/icones/beleza.png" alt="">
                    <p>Beleza</p>                    
                </div>
                <div class="col-6 col-md-4 col-lg-2"><!--  Consultoria  -->
                    <img src="./img/icones/consultoria.png" alt="">
                    <p>Consultoria</p>                    
                </div>
                <div class="col-6 col-md-4 col-lg-2"><!-- Esporte  -->
                    <img src="./img/icones/esporte.png" alt="">
                    <p>Esporte</p>                    
                </div>
                <div class="col-6 col-md-3 col-lg-2"><!-- Gastronomia   -->
                    <img src="./img/icones/gastronomia.png" alt="">
                    <p>Gastronomia</p>                    
                </div>
                <div class="col-6 col-md-3 col-lg-2"><!-- Manutencao   -->
                    <img src="./img/icones/manutencao.png" alt="">
                    <p>Manutenção</p>                    
                </div>
                <div class="col-6 col-md-3 col-lg-2"><!-- Moda  -->
                    <img src="./img/icones/moda.png" alt="">
                    <p>Moda</p>                    
                </div>
                <div class="col-12 col-md-3 col-lg-2"><!-- Saúde   -->
                    <img src="./img/icones/saude.png" alt="">
                    <p>Saúde</p>                    
                </div>
            </div>
      </div><!-- Fim Categoria  -->
      <div class="container-fluid"><!-- Inicio melhores videos  -->
            <div class="row modelos-videos">
                <div class="col-12  text-center">
                    <h2>OS MELHORES VÍDEOS PARA VOCÊ</h2>
                </div>
                <div class="col-12 col-md-6 d-flex flex-column
  justify-content-center align-items-center col-xl-3"><!-- Video 1  -->
                    <img src="./img/img-videos/Institucional_Modelo.jpg" alt="">
                    <a href="">Modelo de Teste 01</a>
                    <p>A partir de: R$299,90</p>
                </div>
                <!-- Fim Video 1  -->
                <div class="col-12 col-md-6 flex-column
 d-flex justify-content-center align-items-center col-xl-3"><!-- Video 2  -->
                    <img src="./img/img-videos/Institucional_Modelo.jpg" alt="">
                    <a href="">Modelo de Teste 02</a>
                    <p>A partir de: R$299,90</p>
                </div>
                <!--Fim Video 2  -->
                <div class="col-12 col-md-6 flex-column
 d-flex justify-content-center align-items-center col-xl-3"><!-- Video 3  -->
                    <img src="./img/img-videos/comunicacao.jpg" alt="">
                    <a href="">Modelo de Teste 03</a>
                    <p>A partir de: R$299,90</p>
                </div>
                <!--Fim Video 3  -->
                <div class="col-12 col-md-6 d-flex flex-column
 justify-content-center align-items-center col-xl-3"><!-- Video 4  -->
                    <img src="./img/img-videos/comunicacao.jpg" alt="">
                    <a href="">Modelo de Teste 04</a>
                    <p>A partir de: R$299,90</p>
                </div>
                <!--Fim Video 4  -->
                <div class="col-12 text-center box-btn-ver">
                    <a href="" class="btn-ver">Ver todos os Vídeos <i class="fas fa-arrow-right"></i></a>
                </div>
            </div>
      </div>
      <div class="container-fluid d-block d-lg-none   carroucel-mobile"><!--Inicio carroucel mobile -->
          <h2 class="text-center">Por que produzir vídeos?</h2>
          <div class="row carroucel ">
              <div class="col-12 ">
                    <p class="hoje text-center">HOJE,vídeo é o caminho mais utilizado por todas as empresas sejam elas micro,pequenas,médias ou grandes  para atingir seu público-alvo.</p>
              </div>
              <div class="col-12 d-flex  flex-column align-items-center">
                    <img src="./img/icones/ico_1.png" alt="">
                    <p class="text-center">Usuários retém 95% das informações em vídeo e apenas 10% em texto.</p>
              </div>
              <div class="col-12 d-flex flex-column align-items-center">
                    <img src="./img/icones/ico_2.png" alt="">
                    <p class="text-center">1% das pessoas são convencidas a comprar um produto ou serviço após assistir a um vídeo.</p>
              </div>
              <div class="col-12 d-flex flex-column align-items-center">
                    <img src="./img/icones/ico_3.png" alt="">
                    <p class="text-center">46% dos usuários têm alguma interação com o conteúdo, após assistir ao vídeo.</p>
              </div>
              <div class="col-12 d-flex flex-column align-items-center">
                    <img src="./img/icones/ico_4.png" alt="">
                    <p class="text-center">90% dos usuários acham que vídeos de produtos contribuem na decisão de compra.</p>
              </div>
          </div>
      </div><!--Fim carroucel mobile -->
      <div class="container-fluid d-none d-lg-block position-relative produzir-video"><!--Inicio container desk-->
          
          <div class="row position-absolute linha-icones ">
              <div class="col-12 col-lg-5 d-flex  flex-column align-items-center">
                    <img src="./img/icones/ico_1.png" alt="">
                    <p class="text-center">Usuários retém 95% das informações em vídeo e apenas 10% em texto.</p>
              </div>
              <div class="col-12 col-lg-5 d-flex flex-column align-items-center">
                    <img src="./img/icones/ico_2.png" alt="">
                    <p class="text-center">1% das pessoas são convencidas a comprar um produto ou serviço após assistir a um vídeo.</p>
              </div>
              <div class="col-12 col-lg-5  d-flex flex-column align-items-center">
                    <img src="./img/icones/ico_3.png" alt="">
                    <p class="text-center">46% dos usuários têm alguma interação com o conteúdo, após assistir ao vídeo.</p>
              </div>
              <div class="col-12 col-lg-5 d-flex flex-column align-items-center">
                    <img src="./img/icones/ico_4.png" alt="">
                    <p class="text-center">90% dos usuários acham que vídeos de produtos contribuem na decisão de compra.</p>
              </div>
          </div>
      </div><!--Fim container desk-->
  </main>

    

<?php
  include 'footer/index.php';
?>